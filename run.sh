#!/bin/bash

$(which docker) run -v "$1:/app" --cpus="12.0" --cpuset-cpus="0-11" -m 8000M -t "$2" /bin/sh -c "cd /app && /analyzer run && chmod 777 $3"
