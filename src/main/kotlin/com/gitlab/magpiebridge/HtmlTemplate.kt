package com.gitlab.magpiebridge;

import com.google.gson.Gson

class HtmlTemplate {

    class Entry (
        val file: String,
        val startLine: Int,
        val endLine: Int,
        val description: String,
        val cwes: String,
        val severity: String
    )
    companion object {
    fun generate(findings: MutableSet<GitLabFinding>): String {
        val table = StringBuilder()
        val labels = HashMap<String, Int>()

        val gson = Gson()

        for(f in findings) {
            val entry = Entry(f.fileName, f.startLine, f.endLine, f.msg, f.cwes.joinToString(","), f.severity)
            table.append(gson.toJson(entry) + ",\n")

            //table.append("{ file: \"${f.fileName}\", startLine: ${f.startLine}, endLine: ${f.endLine}, description : \"${f.msg}\", cwes: \"${f.cwes.joinToString(",")}\", severity: \"${f.severity}\"},\n")
            if (!labels.keys.contains(f.msg)) {
                labels.put(f.msg, 0);
            }
            labels.put(f.msg, labels.get(f.msg)!!.plus(1));
        }
        return """
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />

        <link href="https://unpkg.com/tabulator-tables/dist/css/tabulator.min.css" rel="stylesheet">
        <script type="text/javascript" src="https://unpkg.com/tabulator-tables/dist/js/tabulator.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <style>
/*! tailwindcss v2.2.17 | MIT License | https://tailwindcss.com */

/*! modern-normalize v1.1.0 | MIT License | https://github.com/sindresorhus/modern-normalize */

/*
   Document
       ========
 */

/**
  Use a better box model (opinionated).
 */

        *,
        ::before,
        ::after {
            box-sizing: border-box;
        }

        /**
          Use a more readable tab size (opinionated).
         */

        html {
            -moz-tab-size: 4;
            -o-tab-size: 4;
            tab-size: 4;
        }

        /**
          1. Correct the line height in all browsers.
          2. Prevent adjustments of font size after orientation changes in iOS.
         */

        html {
            line-height: 1.15;
            /* 1 */
            -webkit-text-size-adjust: 100%;
            /* 2 */
        }

        /*
           Sections
               ========
         */

        /**
          Remove the margin in all browsers.
         */

        body {
            margin: 0;
        }

        /**
          Improve consistency of default fonts in all browsers. (https://github.com/sindresorhus/modern-normalize/issues/3)
         */

        body {
            font-family:
                system-ui,
            -apple-system, /* Firefox supports this but not yet `system-ui` */
            'Segoe UI',
            Roboto,
            Helvetica,
            Arial,
            sans-serif,
            'Apple Color Emoji',
            'Segoe UI Emoji';
        }

        /*
           Grouping content
               ================
         */

        /**
          1. Add the correct height in Firefox.
          2. Correct the inheritance of border color in Firefox. (https://bugzilla.mozilla.org/show_bug.cgi?id=190655)
         */

        hr {
            height: 0;
            /* 1 */
            color: inherit;
            /* 2 */
        }

        /*
           Text-level semantics
               ====================
         */

        /**
          Add the correct text decoration in Chrome, Edge, and Safari.
         */

        abbr[title] {
            -webkit-text-decoration: underline dotted;
            text-decoration: underline dotted;
        }

        /**
          Add the correct font weight in Edge and Safari.
         */

        b,
        strong {
            font-weight: bolder;
        }

        /**
          1. Improve consistency of default fonts in all browsers. (https://github.com/sindresorhus/modern-normalize/issues/3)
          2. Correct the odd 'em' font sizing in all browsers.
         */

        code,
        kbd,
        samp,
        pre {
            font-family:
                ui-monospace,
            SFMono-Regular,
            Consolas,
            'Liberation Mono',
            Menlo,
            monospace;
        /* 1 */
        font-size: 1em;
        /* 2 */
        }

        /**
          Add the correct font size in all browsers.
         */

        small {
            font-size: 80%;
        }

        /**
          Prevent 'sub' and 'sup' elements from affecting the line height in all browsers.
         */

        sub,
        sup {
            font-size: 75%;
            line-height: 0;
            position: relative;
            vertical-align: baseline;
        }

        sub {
            bottom: -0.25em;
        }

        sup {
            top: -0.5em;
        }

        /*
           Tabular data
               ============
         */

        /**
          1. Remove text indentation from table contents in Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=999088, https://bugs.webkit.org/show_bug.cgi?id=201297)
          2. Correct table border color inheritance in all Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=935729, https://bugs.webkit.org/show_bug.cgi?id=195016)
         */

        table {
            text-indent: 0;
            /* 1 */
            border-color: inherit;
            /* 2 */
        }

        /*
           Forms
               =====
         */

        /**
          1. Change the font styles in all browsers.
          2. Remove the margin in Firefox and Safari.
         */

        button,
        input,
        optgroup,
        select,
        textarea {
            font-family: inherit;
            /* 1 */
            font-size: 100%;
            /* 1 */
            line-height: 1.15;
            /* 1 */
            margin: 0;
            /* 2 */
        }

        /**
          Remove the inheritance of text transform in Edge and Firefox.
          1. Remove the inheritance of text transform in Firefox.
         */

        button,
        select {
            /* 1 */
            text-transform: none;
        }

        /**
          Correct the inability to style clickable types in iOS and Safari.
         */

        button,
        [type='button'] {
            -webkit-appearance: button;
        }

        /**
          Remove the inner border and padding in Firefox.
         */

        /**
          Restore the focus styles unset by the previous rule.
         */

        /**
          Remove the additional ':invalid' styles in Firefox.
      See: https://github.com/mozilla/gecko-dev/blob/2f9eacd9d3d995c937b4251a5557d95d494c9be1/layout/style/res/forms.css#L728-L737
         */

        /**
          Remove the padding so developers are not caught out when they zero out 'fieldset' elements in all browsers.
         */

        legend {
            padding: 0;
        }

        /**
          Add the correct vertical alignment in Chrome and Firefox.
         */

        progress {
            vertical-align: baseline;
        }

        /**
          Correct the cursor style of increment and decrement buttons in Safari.
         */

        /**
          1. Correct the odd appearance in Chrome and Safari.
          2. Correct the outline style in Safari.
         */

        /**
          Remove the inner padding in Chrome and Safari on macOS.
         */

        /**
          1. Correct the inability to style clickable types in iOS and Safari.
          2. Change font properties to 'inherit' in Safari.
         */

        /*
           Interactive
               ===========
         */

        /*
           Add the correct display in Chrome and Safari.
         */

        summary {
            display: list-item;
        }

        /**
         * Manually forked from SUIT CSS Base: https://github.com/suitcss/base
         * A thin layer on top of normalize.css that provides a starting point more
         * suitable for web applications.
         */

        /**
         * Removes the default spacing and border for appropriate elements.
         */

        blockquote,
        dl,
        dd,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        hr,
        figure,
        p,
        pre {
            margin: 0;
        }

        button {
            background-color: transparent;
            background-image: none;
        }

        fieldset {
            margin: 0;
            padding: 0;
        }

        ol,
        ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        /**
         * Tailwind custom reset styles
         */

        /**
         * 1. Use the user's configured `sans` font-family (with Tailwind's default
         *    sans-serif font stack as a fallback) as a sane default.
         * 2. Use Tailwind's default "normal" line-height so the user isn't forced
         *    to override it to ensure consistency even when using the default theme.
         */

        html {
            font-family: "Source Sans Pro", ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            /* 1 */
            line-height: 1.5;
            /* 2 */
        }

        /**
         * Inherit font-family and line-height from `html` so users can set them as
         * a class directly on the `html` element.
         */

        body {
            font-family: inherit;
            line-height: inherit;
        }

        /**
         * 1. Prevent padding and border from affecting element width.
         *
         *    We used to set this in the html element and inherit from
         *    the parent element for everything else. This caused issues
         *    in shadow-dom-enhanced elements like <details> where the content
         *    is wrapped by a div with box-sizing set to `content-box`.
         *
         *    https://github.com/mozdevs/cssremedy/issues/4
         *
         *
         * 2. Allow adding a border to an element by just adding a border-width.
         *
         *    By default, the way the browser specifies that an element should have no
         *    border is by setting it's border-style to `none` in the user-agent
         *    stylesheet.
         *
         *    In order to easily add borders to elements by just setting the `border-width`
         *    property, we change the default border-style for all elements to `solid`, and
         *    use border-width to hide them instead. This way our `border` utilities only
         *    need to set the `border-width` property instead of the entire `border`
         *    shorthand, making our border utilities much more straightforward to compose.
         *
         *    https://github.com/tailwindcss/tailwindcss/pull/116
         */

*,
::before,
::after {
    box-sizing: border-box;
    /* 1 */
    border-width: 0;
    /* 2 */
    border-style: solid;
    /* 2 */
    border-color: currentColor;
    /* 2 */
}

                /*
                 * Ensure horizontal rules are visible by default
                 */

                hr {
                    border-top-width: 1px;
                }

                /**
                 * Undo the `border-style: none` reset that Normalize applies to images so that
                 * our `border-{width}` utilities have the expected effect.
                 *
                 * The Normalize reset is unnecessary for us since we default the border-width
                 * to 0 on all elements.
                 *
                 * https://github.com/tailwindcss/tailwindcss/issues/362
                 */

                img {
                    border-style: solid;
                }

                textarea {
                    resize: vertical;
                }

                input::-moz-placeholder, textarea::-moz-placeholder {
                    opacity: 1;
                    color: #9ca3af;
                }

                input:-ms-input-placeholder, textarea:-ms-input-placeholder {
                    opacity: 1;
                    color: #9ca3af;
                }

                input::placeholder,
                textarea::placeholder {
                    opacity: 1;
                    color: #9ca3af;
                }

                button {
                    cursor: pointer;
                }

                /**
                 * Override legacy focus reset from Normalize with modern Firefox focus styles.
                 *
                 * This is actually an improvement over the new defaults in Firefox in our testing,
                 * as it triggers the better focus styles even for links, which still use a dotted
                 * outline in Firefox by default.
                 */

                table {
                    border-collapse: collapse;
                }

                h1,
                h2,
                h3,
                h4,
                h5,
                h6 {
                    font-size: inherit;
                    font-weight: inherit;
                }

                /**
                 * Reset links to optimize for opt-in styling instead of
                 * opt-out.
                 */

                a {
                    color: inherit;
                    text-decoration: inherit;
                }

                /**
                 * Reset form element properties that are easy to forget to
                 * style explicitly so you don't inadvertently introduce
                 * styles that deviate from your design system. These styles
                 * supplement a partial reset that is already applied by
                 * normalize.css.
                 */

                button,
                input,
                optgroup,
                select,
                textarea {
                    padding: 0;
                    line-height: inherit;
                    color: inherit;
                }

                /**
                 * Use the configured 'mono' font family for elements that
                 * are expected to be rendered with a monospace font, falling
                 * back to the system monospace stack if there is no configured
                 * 'mono' font family.
                 */

                pre,
                code,
                kbd,
                samp {
                    font-family: ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
                }

                /**
                 * 1. Make replaced elements `display: block` by default as that's
                 *    the behavior you want almost all of the time. Inspired by
                 *    CSS Remedy, with `svg` added as well.
                 *
                 *    https://github.com/mozdevs/cssremedy/issues/14
                 *
                 * 2. Add `vertical-align: middle` to align replaced elements more
                 *    sensibly by default when overriding `display` by adding a
                 *    utility like `inline`.
                 *
                 *    This can trigger a poorly considered linting error in some
                 *    tools but is included by design.
                 *
                 *    https://github.com/jensimmons/cssremedy/issues/14#issuecomment-634934210
                 */

                img,
                svg,
                video,
                canvas,
                audio,
                iframe,
                embed,
                object {
                    display: block;
                    /* 1 */
                    vertical-align: middle;
                    /* 2 */
                }

                /**
                 * Constrain images and videos to the parent width and preserve
                 * their intrinsic aspect ratio.
                 *
                 * https://github.com/mozdevs/cssremedy/issues/14
                 */

                img,
                video {
                    max-width: 100%;
                    height: auto;
                }

                /**
                 * Ensure the default browser behavior of the `hidden` attribute.
                 */

                [hidden] {
                    display: none;
                }

*, ::before, ::after {
    --tw-border-opacity: 1;
    border-color: rgba(229, 231, 235, var(--tw-border-opacity));
}

                .container {
                    width: 100%;
                }

                @media (min-width: 640px) {
                    .container {
                        max-width: 640px;
                    }
                }

                @media (min-width: 768px) {
                    .container {
                        max-width: 768px;
                    }
                }

                @media (min-width: 1024px) {
                    .container {
                        max-width: 1024px;
                    }
                }

                @media (min-width: 1280px) {
                    .container {
                        max-width: 1280px;
                    }
                }

                @media (min-width: 1536px) {
                    .container {
                        max-width: 1536px;
                    }
                }

                .relative {
                    position: relative;
                }

                .mx-4 {
                    margin-left: 1rem;
                    margin-right: 1rem;
                }

                .mt-4 {
                    margin-top: 1rem;
                }

                .mt-8 {
                    margin-top: 2rem;
                }

                .mr-2 {
                    margin-right: 0.5rem;
                }

                .mr-6 {
                    margin-right: 1.5rem;
                }

                .mb-0 {
                    margin-bottom: 0px;
                }

                .mb-1 {
                    margin-bottom: 0.25rem;
                }

                .mb-6 {
                    margin-bottom: 1.5rem;
                }

                .-mb-px {
                    margin-bottom: -1px;
                }

                .ml-2 {
                    margin-left: 0.5rem;
                }

                .ml-4 {
                    margin-left: 1rem;
                }

                .block {
                    display: block;
                }

                .inline-block {
                    display: inline-block;
                }

                .flex {
                    display: flex;
                }

                .table {
                    display: table;
                }

                .hidden {
                    display: none;
                }

                .h-3 {
                    height: 0.75rem;
                }

                .min-h-screen {
                    min-height: 100vh;
                }

                .w-3 {
                    width: 0.75rem;
                }

                .w-80 {
                    width: 20rem;
                }

                .w-full {
                    width: 100%;
                }

                .min-w-0 {
                    min-width: 0px;
                }

                .flex-auto {
                    flex: 1 1 auto;
                }

                .flex-shrink-0 {
                    flex-shrink: 0;
                }

                .flex-grow {
                    flex-grow: 1;
                }

                @-webkit-keyframes spin {
                    to {
                        transform: rotate(360deg);
                    }
                }

                @keyframes spin {
                    to {
                        transform: rotate(360deg);
                    }
                }

                @-webkit-keyframes ping {
                    75%, 100% {
                        transform: scale(2);
                        opacity: 0;
                    }
                }

                @keyframes ping {
                    75%, 100% {
                        transform: scale(2);
                        opacity: 0;
                    }
                }

                @-webkit-keyframes pulse {
                    50% {
                        opacity: .5;
                    }
                }

                @keyframes pulse {
                    50% {
                        opacity: .5;
                    }
                }

                @-webkit-keyframes bounce {
                    0%, 100% {
                        transform: translateY(-25%);
                        -webkit-animation-timing-function: cubic-bezier(0.8,0,1,1);
                        animation-timing-function: cubic-bezier(0.8,0,1,1);
                    }

                    50% {
                        transform: none;
                        -webkit-animation-timing-function: cubic-bezier(0,0,0.2,1);
                        animation-timing-function: cubic-bezier(0,0,0.2,1);
                    }
                }

                @keyframes bounce {
                    0%, 100% {
                        transform: translateY(-25%);
                        -webkit-animation-timing-function: cubic-bezier(0.8,0,1,1);
                        animation-timing-function: cubic-bezier(0.8,0,1,1);
                    }

                    50% {
                        transform: none;
                        -webkit-animation-timing-function: cubic-bezier(0,0,0.2,1);
                        animation-timing-function: cubic-bezier(0,0,0.2,1);
                    }
                }

                .list-none {
                    list-style-type: none;
                }

                .flex-row {
                    flex-direction: row;
                }

                .flex-col {
                    flex-direction: column;
                }

                .flex-wrap {
                    flex-wrap: wrap;
                }

                .items-center {
                    align-items: center;
                }

                .justify-end {
                    justify-content: flex-end;
                }

                .justify-center {
                    justify-content: center;
                }

                .justify-between {
                    justify-content: space-between;
                }

                .break-words {
                    overflow-wrap: break-word;
                }

                .rounded {
                    border-radius: 0.25rem;
                }

                .rounded-lg {
                    border-radius: 0.5rem;
                }

                .rounded-t-lg {
                    border-top-left-radius: 0.5rem;
                    border-top-right-radius: 0.5rem;
                }

                .border {
                    border-width: 1px;
                }

                .border-white {
                    --tw-border-opacity: 1;
                    border-color: rgba(255, 255, 255, var(--tw-border-opacity));
                }

                .hover\:border-transparent:hover {
                    border-color: transparent;
                }

                .hover\:border-white:hover {
                    --tw-border-opacity: 1;
                    border-color: rgba(255, 255, 255, var(--tw-border-opacity));
                }

                .bg-white {
                    --tw-bg-opacity: 1;
                    background-color: rgba(255, 255, 255, var(--tw-bg-opacity));
                }

                .bg-gray-100 {
                    --tw-bg-opacity: 1;
                    background-color: rgba(243, 244, 246, var(--tw-bg-opacity));
                }

                .bg-gray-200 {
                    --tw-bg-opacity: 1;
                    background-color: rgba(229, 231, 235, var(--tw-bg-opacity));
                }

                .bg-gray-300 {
                    --tw-bg-opacity: 1;
                    background-color: rgba(209, 213, 219, var(--tw-bg-opacity));
                }

                .bg-gray-600 {
                    --tw-bg-opacity: 1;
                    background-color: rgba(75, 85, 99, var(--tw-bg-opacity));
                }

                .bg-gray-800 {
                    --tw-bg-opacity: 1;
                    background-color: rgba(31, 41, 55, var(--tw-bg-opacity));
                }

                .bg-yellow-600 {
                    --tw-bg-opacity: 1;
                    background-color: rgba(217, 119, 6, var(--tw-bg-opacity));
                }

                .hover\:bg-white:hover {
                    --tw-bg-opacity: 1;
                    background-color: rgba(255, 255, 255, var(--tw-bg-opacity));
                }

                .fill-current {
                    fill: currentColor;
                }

                .p-2 {
                    padding: 0.5rem;
                }

                .px-2 {
                    padding-left: 0.5rem;
                    padding-right: 0.5rem;
                }

                .px-3 {
                    padding-left: 0.75rem;
                    padding-right: 0.75rem;
                }

                .px-4 {
                    padding-left: 1rem;
                    padding-right: 1rem;
                }

                .px-5 {
                    padding-left: 1.25rem;
                    padding-right: 1.25rem;
                }

                .py-1 {
                    padding-top: 0.25rem;
                    padding-bottom: 0.25rem;
                }

                .py-2 {
                    padding-top: 0.5rem;
                    padding-bottom: 0.5rem;
                }

                .py-3 {
                    padding-top: 0.75rem;
                    padding-bottom: 0.75rem;
                }

                .py-5 {
                    padding-top: 1.25rem;
                    padding-bottom: 1.25rem;
                }

                .pt-3 {
                    padding-top: 0.75rem;
                }

                .pb-4 {
                    padding-bottom: 1rem;
                }

                .text-center {
                    text-align: center;
                }

                .font-sans {
                    font-family: "Source Sans Pro", ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                }

                .text-xs {
                    font-size: 0.75rem;
                    line-height: 1rem;
                }

                .text-sm {
                    font-size: 0.875rem;
                    line-height: 1.25rem;
                }

                .text-lg {
                    font-size: 1.125rem;
                    line-height: 1.75rem;
                }

                .text-2xl {
                    font-size: 1.5rem;
                    line-height: 2rem;
                }

                .font-bold {
                    font-weight: 700;
                }

                .uppercase {
                    text-transform: uppercase;
                }

                .leading-none {
                    line-height: 1;
                }

                .leading-normal {
                    line-height: 1.5;
                }

                .text-white {
                    --tw-text-opacity: 1;
                    color: rgba(255, 255, 255, var(--tw-text-opacity));
                }

                .text-yellow-600 {
                    --tw-text-opacity: 1;
                    color: rgba(217, 119, 6, var(--tw-text-opacity));
                }

                .text-blue-100 {
                    --tw-text-opacity: 1;
                    color: rgba(219, 234, 254, var(--tw-text-opacity));
                }

                .hover\:text-black:hover {
                    --tw-text-opacity: 1;
                    color: rgba(0, 0, 0, var(--tw-text-opacity));
                }

                .hover\:text-white:hover {
                    --tw-text-opacity: 1;
                    color: rgba(255, 255, 255, var(--tw-text-opacity));
                }

                .hover\:underline:hover {
                    text-decoration: underline;
                }

*, ::before, ::after {
    --tw-shadow: 0 0 #0000;
}

                .shadow-lg {
                    --tw-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05);
                    box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
                }

*, ::before, ::after {
    --tw-ring-inset: var(--tw-empty,/*!*/ /*!*/);
    --tw-ring-offset-width: 0px;
    --tw-ring-offset-color: #fff;
    --tw-ring-color: rgba(59, 130, 246, 0.5);
    --tw-ring-offset-shadow: 0 0 #0000;
    --tw-ring-shadow: 0 0 #0000;
}

                .hyperlink {
                    --tw-text-opacity: 1;
                    color: rgba(37, 99, 235, var(--tw-text-opacity));
                    text-decoration: underline;
                }

                .hyperlink:visited {
                    --tw-text-opacity: 1;
                    color: rgba(124, 58, 237, var(--tw-text-opacity));
                }

                pre {
                    white-space: pre-wrap;
                }

                ul {
                    list-style-type: square;
                    margin-left: 1.5rem;
                }

                ol {
                    list-style-type: decimal;
                    margin-left: 1.5rem;
                }

                @media (min-width: 640px) {
                }

                @media (min-width: 768px) {
                }

                @media (min-width: 1024px) {
                    .lg\:mt-0 {
                        margin-top: 0px;
                    }

                    .lg\:flex {
                        display: flex;
                    }

                    .lg\:hidden {
                        display: none;
                    }

                    .lg\:w-auto {
                        width: auto;
                    }

                    .lg\:w-10\/12 {
                        width: 83.333333%;
                    }
                }

                @media (min-width: 1280px) {
                }

                @media (min-width: 1536px) {
                }


        </style>
        <title>GitLab Mux Stats</title>
    </head>
    <body class="bg-gray-100 font-sans">
        <nav class="flex items-center justify-between flex-wrap bg-gray-800 p-2">
            <div class="flex items-center flex-shrink-0 text-white mr-6">
                <svg width="24" height="24" class="tanuki-logo" viewBox="0 0 36 36">
                    <path
                    class="tanuki-shape tanuki-left-ear"
                    fill="#e24329"
                    d="M2 14l9.38 9v-9l-4-12.28c-.205-.632-1.176-.632-1.38 0z"
                    ></path>
                    <path
                    class="tanuki-shape tanuki-right-ear"
                    fill="#e24329"
                    d="M34 14l-9.38 9v-9l4-12.28c.205-.632 1.176-.632 1.38 0z"
                    ></path>
                    <path
                    class="tanuki-shape tanuki-nose"
                    fill="#e24329"
                    d="M18,34.38 3,14 33,14 Z"
                    ></path>
                    <path
                    class="tanuki-shape tanuki-left-eye"
                    fill="#fc6d26"
                    d="M18,34.38 11.38,14 2,14 6,25Z"
                    ></path>
                    <path
                    class="tanuki-shape tanuki-right-eye"
                    fill="#fc6d26"
                    d="M18,34.38 24.62,14 34,14 30,25Z"
                    ></path>
                    <path
                    class="tanuki-shape tanuki-left-cheek"
                    fill="#fca326"
                    d="M2 14L.1 20.16c-.18.565 0 1.2.5 1.56l17.42 12.66z"
                    ></path>
                    <path
                    class="tanuki-shape tanuki-right-cheek"
                    fill="#fca326"
                    d="M34 14l1.9 6.16c.18.565 0 1.2-.5 1.56L18 34.38z"
                    ></path>
                </svg>
            </div>
            <div class="block lg:hidden">
                <button
                    class="
                    flex
                    items-center
                    px-3
                    py-2
                    border
                    rounded
                    text-teal-200
                    border-teal-400
                    hover:text-white hover:border-white
                    "
                    >
                    <svg
                        class="fill-current h-3 w-3"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                        >
                        <title>Menu</title>
                        <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
                    </svg>
                </button>
            </div>
            <div class="text-lg text-white">
                GitLab Mux Statistics
            </div>
            <div class="w-full block flex-grow lg:flex justify-end lg:w-auto">
                <div>
                    <a
                        href="#"
                        class="
                        inline-block
                        text-sm
                        px-4
                        py-2
                        leading-none
                        border
                        rounded
                        text-white
                        border-white
                        hover:border-transparent hover:text-black hover:bg-white
                        mt-4
                        lg:mt-0
                        "
                        >Information</a
                    >
                </div>
            </div>
        </nav>
        <main class="justify-center mx-4 mt-4 w-full">
            <div
                class="
                flex-wrap
                text-2xl
                lg:w-10/12
                mr-2
                ml-2
                mt-4
                shadow-lg
                rounded-lg
                bg-gray-200
                "
                >

                <div
                    id="execution"
                    class="p-2 rounded-t-lg bg-gray-600 text-blue-100"
                    >
                    Findings/Statistics
                </div>

                    <div class="p-2 bg-white">


                        <div id="findings-table"></div>
                        <div style="width:400px;"> <canvas id="myChart"></canvas></div>

                        <script>
                            //define data array
                            var tabledata = [ ${table.toString()}];

                            //initialize table
                            var table = new Tabulator("#findings-table", {
                                                            data:tabledata, //assign data to table
                                                            autoColumns:true, //create columns from data field names
                                                        });
                        </script>
                        
                        <script>
                            const data = {
                              labels: [ ${labels.keys.map { k -> "\"${k}\"" }.joinToString(",")} ],
                              datasets: [{
                                label: 'Vulnerability Distribution',
                                data: [ ${labels.values.joinToString(",") } ],
                                backgroundColor: [
                                  'rgb(255, 99, 132)',
                                  'rgb(54, 162, 235)',
                                  'rgb(255, 205, 86)'
                                ],
                                hoverOffset: 4
                              }]
                            };
                            const config = {
                              type: 'doughnut',
                              data: data,
                            };
                            
                              const myChart = new Chart(
                                document.getElementById('myChart'),
                                config
                              );
                             
                              myChart.resize();
                            </script>


                    </div>
            </div>
            

            </div>
        </main>
        </div>
    </body>
</html>
"""
    }
        }
}