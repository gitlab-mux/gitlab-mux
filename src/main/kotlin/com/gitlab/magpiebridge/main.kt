package com.gitlab.magpiebridge

import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.required
import magpiebridge.command.OpenURLCommand
import magpiebridge.core.*
import org.eclipse.lsp4j.InitializedParams
import org.eclipse.lsp4j.jsonrpc.MessageConsumer
import org.eclipse.lsp4j.jsonrpc.messages.Either
import org.eclipse.lsp4j.jsonrpc.messages.Message
import java.io.File
import java.io.IOException
import java.net.URISyntaxException
import java.util.logging.Logger

private val LOG = Logger.getLogger("main")

fun main(args: Array<String>) {
    val parser = ArgParser("mux")
    val input by parser.option(ArgType.String, shortName = "i", description = "project path").required()
    val cfg by parser.option(ArgType.String, shortName = "c", description = "configuration file").required()
    parser.parse(args)

    if (!File(input).exists() && !File(input).isDirectory) {
        println("Directory $input does not exist")
        kotlin.system.exitProcess(-1)
    }

    if (!File(cfg).exists()) {
        println("configuration file $input does not exist")
        kotlin.system.exitProcess(-1)
    }

    LOG.info("digesting $input")

    val config = Utils.parseTomlConfig(File(cfg).readText(Charsets.UTF_8))

    LOG.info("tmpdir is ${config.tmpdir}")

    // launch the server, here we choose stand I/O. Note later don't use System.out
    // to print text messages to console, it will block the channel.
    //createServer(preparedFile).launchOnStdio();

    // for debugging
    MagpieServer.launchOnSocketPort(5007) { createServer(input, config) }
    // launch the server, here we choose stand I/O. Note later don't use System.out
    // to print text messages to console, it will block the channel.

    // launch the server, here we choose stand I/O. Note later don't use System.out
    // to print text messages to console, it will block the channel.
    //createServer()!!.launchOnStdio()
}

private fun createServer(projectPath :String, config: Configuration): MagpieServer? {
    // set up configuration for MagpieServer
    class LanguageHandler : LanguageExtensionHandler {
        override fun getLanguageForExtension(extension: String?): String {
            if (extension == "go") {
                return "go"
            }
            return ""
        }

        override fun getExtensionsForLanguage(language: String?): MutableSet<String> {
            if (language == "go")
                return mutableSetOf(".go")
            return mutableSetOf()
        }
    }

    class myConfig : ServerConfiguration() {
        override fun getLanguageExtensionHandler(): LanguageExtensionHandler {
            return LanguageHandler()
        }
    }

    class consumer : MessageConsumer {
        override fun consume(message: Message) {
            LOG.info(message.toString())
        }
    }

    val serverConfig = myConfig()
    serverConfig.doAnalysisBySave()
    serverConfig.setUseMagpieHTTPServer(true)
    serverConfig.setShowConfigurationPage(true, true)


    class myServer : MagpieServer(serverConfig) {
        val httpServer = HttpServer()

        override fun initialized(params: InitializedParams) {
            LOG.info("initialized")
            super.initialized(params)
        }

        override fun createAndStartLocalHttpServer() {
            LOG.info("create and start local HTTP server")
            try {
                initAnalysisConfiguration()
                val url = httpServer.createAndStartLocalHttpServer(this)
                OpenURLCommand.showHTMLinClientOrBroswer(this, client, url)
            } catch (e: IOException) {
                LOG.warning(e.message)
                e.printStackTrace()
            } catch (e: URISyntaxException) {
                LOG.warning(e.message)
                e.printStackTrace()
            }
        }
    }

    val server = myServer()
    val myAnalysis: ServerAnalysis = GitLabAnalysis(projectPath, config, server.httpServer)
    val analysis = Either.forLeft<ServerAnalysis, ToolAnalysis>(myAnalysis)
    server.addAnalysis(analysis, "java", "go", "python")
    return server
}
