package com.gitlab.magpiebridge

import com.ibm.wala.cast.tree.CAstSourcePositionMap
import com.ibm.wala.classLoader.IMethod
import com.ibm.wala.classLoader.Module
import com.ibm.wala.classLoader.SourceFileModule
import com.ibm.wala.util.collections.Pair
import magpiebridge.core.*
import magpiebridge.util.SourceCodeReader
import org.eclipse.lsp4j.DiagnosticSeverity
import java.io.File
import java.io.IOException
import java.io.Reader
import java.net.MalformedURLException
import java.net.URL
import java.util.logging.Logger

class GitLabAnalysis(val projectPath: String, val config: Configuration, val httpServer: HttpServer) : ServerAnalysis {
    private val LOG = Logger.getLogger(GitLabAnalysis::class.simpleName)
    private val results: MutableSet<GitLabFinding> = HashSet()

    override fun source() = "GitLab Mux"

    private fun readResults(tmpdir: String, filter: Set<String> = HashSet()) {
        LOG.info("reread results")
        results.addAll(Utils.extractResults(tmpdir, filter))
    }

    override fun analyze(files: Collection<Module>, server: AnalysisConsumer, rerun: Boolean) {
        LOG.info("rerun analysis: " + rerun)
        this.results.clear()
        try {
            if (rerun) {
                var xt = HashSet<String>()
                //var analyers = HashSet<String>()
                for (file in files) {
                    LOG.info("File is ${ file.toString() }")

                    val ext = File(file.toString()).extension.removePrefix(".")
                    LOG.info("looking for $ext")
                    if (config.entries.contains(ext)) {
                        val analyzer = config.entries[ext]!!
                        Utils.runAnalysis(projectPath, analyzer.url, analyzer.name, config.tmpdir)
                        xt.add(ext)
                        //analyers.add(analyzer.name)
                    }
                }
                readResults(config.tmpdir, xt)
                val results: Set<AnalysisResult> = runAnalysisOnSelectedFiles(files, server as MagpieServer)
                server.consume(results, source())
                val handler = httpServer.context.handler
                if (handler is HttpHandler) {
                    handler.findings = this.results
                }
            }
        } catch (e: MalformedURLException) {
            throw RuntimeException(e)
        }
    }

    @Throws(MalformedURLException::class)
    fun runAnalysisOnSelectedFiles(files: Collection<Module>, server: MagpieServer): Set<AnalysisResult> {
        val results: MutableSet<AnalysisResult> = HashSet()
        for (file in files) {
            if (file is SourceFileModule) {
                for (res in this.results) {
                    val isSupported = arrayOf("go", "java", "py").any {
                        res.fileName.endsWith(it)
                    }
                    if (isSupported) {
                        val clientURL = URL(server.getClientUri(file.url.toString()))
                        val pos: CAstSourcePositionMap.Position = object : CAstSourcePositionMap.Position {
                            override fun getFirstCol() =
                                Utils.columnsFromLine(this.url, this.firstLine, this.lastLine)
                                    .left

                            override fun getLastCol() =
                                Utils.columnsFromLine(this.url, this.firstLine, this.lastLine)
                                    .right

                            @Throws(IOException::class)
                            override fun getReader(): Reader? {
                                return null
                            }

                            override fun compareTo(arg0: IMethod.SourcePosition) = 0
                            override fun getFirstLine() = res.startLine
                            override fun getLastLine() = res.endLine
                            override fun getFirstOffset() = 0
                            override fun getLastOffset() = 0
                            override fun getURL() = clientURL
                        }
                        val r: AnalysisResult = convert(res, pos)
                        results.add(r)
                    }
                }
            }
        }
        return results
    }


    private fun convert(result: GitLabFinding, pos: CAstSourcePositionMap.Position): AnalysisResult {
        return object : AnalysisResult {
            override fun toString(useMarkdown: Boolean) = result.msg
            override fun severity(): DiagnosticSeverity = DiagnosticSeverity.Error
            override fun repair() = Pair.make(pos, result.repair)
            override fun related(): Iterable<Pair<CAstSourcePositionMap.Position, String>> {
                return ArrayList()
            }
            override fun position() = pos
            override fun kind() =Kind.Diagnostic
            override fun code(): String {
                var code: String? = null
                try {
                    code = SourceCodeReader.getLinesInString(pos)
                } catch (e: Exception) {
                }
                if (code != null) {
                    return code
                }
                return ""
            }
        }
    }
}