package com.gitlab.magpiebridge

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.io.path.pathString
import kotlin.io.path.writeText

internal class UtilsKtTest {

    @Test
    fun testRunAnalysis() {
        Utils.runAnalysis("/home/julian/git-repositories/gitlab/magpiebridge/demo-go",
            "registry.gitlab.com/gitlab-org/security-products/analyzers/gosec:latest",
            "test",
            "/tmp/test"
            )
    }

    @Test
    fun testTomlParser() {
        var toml = """    
tmpdir = "/tmp/out"
        
    [[analyzers]]
    name = "gosec"
    url = "registry.gitlab.com/gitlab-org/security-products/analyzers/gosec:latest"
    extensions = "go"
    
    [[analyzers]]
    name = "semgrep"
    url = "registry.gitlab.com/gitlab-org/security-products/analyzers/semgrep:latest"
    extensions = "py,pyt"
        """.trimIndent()
        val config = Utils.parseTomlConfig(toml)
        assertEquals(config.entries.size, 3)
        assertEquals(config.tmpdir, "/tmp/out")
    }

    @Test
    fun testUtils() {
        var code = """
func foo1() {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}
	rows, err := db.Query("SELECT * FROM foo WHERE name = " + os.Args[1])
	if err != nil {
		panic(err)
	}
	//test
	defer rows.Close()
}
    """.trimIndent()

        val tmpfile = kotlin.io.path.createTempFile("tmp")
        tmpfile.writeText(code)
        tmpfile.toUri()
        try {
            val ret = Utils.columnsFromLine(
                tmpfile.toUri().toURL(),
                2,
                2
            )
            assertEquals(ret.left, 1)
            assertEquals(ret.right, 43)
        } catch (e: Exception) {
            e.stackTrace
            println(e.message)
        }
    }

    @Test
    fun testReportTranslate() {
        var report = """
{
  "version": "14.0.4",
  "vulnerabilities": [
    {
      "id": "e4f5a11ead7d0eaec6b119a7dd3553967b963dee221213e063d89eaa30018dfc",
      "category": "sast",
      "message": "Something is wrong here",
      "description": "foo or bar are used",
      "cve": "",
      "severity": "Info",
      "scanner": {
        "id": "semgrep",
        "name": "Semgrep"
      },
      "location": {
        "file": "Example.go",
        "start_line": 20,
        "end_line": 20
      },
      "identifiers": [
        {
          "type": "semgrep_id",
          "name": "alert function foo used",
          "value": "alert function foo used"
        }
      ]
    },
    {
      "id": "e4f5a11ead7d0eaec6b119a7dd3553967b963dee221213e063d89eaa30018dfg",
      "category": "sast",
      "message": "Something else is wrong here",
      "description": "foo or bar are used",
      "cve": "",
      "severity": "Info",
      "scanner": {
        "id": "semgrep",
        "name": "Semgrep"
      },
      "location": {
        "file": "Example.go",
        "start_line": 27,
        "end_line": 27
      },
      "identifiers": [
        {
          "type": "semgrep_id",
          "name": "alert function foo used",
          "value": "alert function foo used"
        }
      ]
    }
  ],
  "scan": {
    "scanner": {
      "id": "semgrep",
      "name": "Semgrep",
      "url": "https://github.com/returntocorp/semgrep",
      "vendor": {
        "name": "GitLab"
      },
      "version": "0.76.2"
    },
    "type": "sast",
    "start_time": "2022-01-19T15:55:56",
    "end_time": "2022-01-19T15:55:58",
    "status": "success"
  }
}
    """.trimIndent()

        val tmpfile = kotlin.io.path.createTempFile("tmp")
        tmpfile.writeText(report)
        tmpfile.toUri()

        assertTrue(Utils.extractResults(tmpfile.pathString).any {
            it.fileName == "Example.go" && it.msg == "Something else is wrong here"
        })
    }
}

